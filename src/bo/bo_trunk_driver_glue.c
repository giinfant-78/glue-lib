/*-----------------------------------------------------------------------
 * trunk_driver_bo.c
 *
 * Feb, 2019, BO Team
 *
 * Copyright (c) 2017-2019 by Cisco Systems, Inc.
 *
 * LC application Trunk driver BO
 *-----------------------------------------------------------------------
 */

#include "bo_trunk_driver.h"
#include "bo_descriptor.h"
#include "mdio_driver.h"
#include "i2c_driver.h"

/*********** START ------ BO GLUE LIB INIT For FlexCOH SDK --------- *************/
/* Below code is added only for FlexCOH SDK to include BO GLUE LIB */

static i2c_dev_info_t i2c_dev_info [] =
{
    {
        .dev_id   =  13,
        .name     = "/dev/i2c-13",
        .devices[I2C_DEV_CFP2_P0_0].dev_addr = I2C_DEV_ADDR_MAX7314_0,
        .devices[I2C_DEV_CFP2_P0_0].addr_size = _1BYTE_ADDR,
        .devices[I2C_DEV_CFP2_P0_1].dev_addr = I2C_DEV_ADDR_MAX7314_1,
        .devices[I2C_DEV_CFP2_P0_1].addr_size = _1BYTE_ADDR,
        .topology = ":"
    },
    {
        .dev_id   =  14,
        .name     = "/dev/i2c-14",
        .devices[I2C_DEV_CFP2_P1_0].dev_addr = I2C_DEV_ADDR_MAX7314_0,
        .devices[I2C_DEV_CFP2_P1_0].addr_size = _1BYTE_ADDR,
        .devices[I2C_DEV_CFP2_P1_1].dev_addr = I2C_DEV_ADDR_MAX7314_1,
        .devices[I2C_DEV_CFP2_P1_1].addr_size = _1BYTE_ADDR,
        .topology = ":"
    },
};

static i2c_cfg_t i2c_config =
{
    .nr_of_i2c_dev = sizeof(i2c_dev_info)/sizeof(i2c_dev_info_t),
    .i2c_devs = i2c_dev_info
};

static mdio_dev_info_t mdio_dev_info [] =
{
    {
        .dev_id   =  4,
        .name     = "/dev/uio10",
        .devices[MDIO_DEV0_CFP2].dev_addr  = MDIO_DEV0_ADDR_CFP2,
        .devices[MDIO_DEV0_CFP2].dev_speed = MDIO_CLOCK_4_MHZ,
        .devices[MDIO_DEV0_CFP2].addr_size = _1BYTE_ADDR,
        .topology = ":"
    },
    {
        .dev_id   =  5,
        .name     = "/dev/uio11",
        .devices[MDIO_DEV0_CFP2].dev_addr  = MDIO_DEV0_ADDR_CFP2,
        .devices[MDIO_DEV0_CFP2].dev_speed = MDIO_CLOCK_4_MHZ,
        .devices[MDIO_DEV0_CFP2].addr_size = _1BYTE_ADDR,
        .topology = ":"
    }
};

static mdio_cfg_t mdio_config =
{
    .nr_of_mdio_dev = sizeof(mdio_dev_info)/sizeof(mdio_dev_info_t),
    .mdio_devs = mdio_dev_info
};

static trunk_cfp2dco_dev_inf_t cfp2_devs[]=
{
    {
       .face_port_id    = TRUNK_DCO_PORT_0,
       .dev_id          = TRUNK_DCO_DEV_0,
       .mdio_hw_id      = 5, //uio11
       .mdio_bus_id     = MDIO_DEV0_CFP2,
       .mxp1_i2c_hw_id  = I2C_DEV_CFP2_P0_0, // Status & Control Read
       .mxp1_i2c_bus_id = 14,
       .mxp2_i2c_hw_id  = I2C_DEV_CFP2_P0_1, // Rx LOS
       .mxp2_i2c_bus_id = 14,
    },
    {
       .face_port_id    = TRUNK_DCO_PORT_1,
       .dev_id          = TRUNK_DCO_DEV_1,
       .mdio_hw_id      = 4, //uio10
       .mdio_bus_id     = MDIO_DEV0_CFP2,
       .mxp1_i2c_hw_id  = I2C_DEV_CFP2_P1_0, // Status & Control Read
       .mxp1_i2c_bus_id = 13,
       .mxp2_i2c_hw_id  = I2C_DEV_CFP2_P1_1, // Rx LOS
       .mxp2_i2c_bus_id = 13,
    }
};

static trunk_hal_glue_callbacks_t trunk_bo_glue_lib_cb =
{
    .read_reg_cb     = trunk_driver_bo_read_reg,
    .write_reg_cb    = trunk_driver_bo_write_reg,
    .read_status_cb  = trunk_driver_bo_read_status_pin,
    .read_ctrl_cb    = trunk_driver_bo_read_ctrl_pin,
    .write_ctrl_cb   = trunk_driver_bo_write_ctrl_pin,
    .set_led_cb      = trunk_driver_bo_set_led,
    .get_led_cb      = trunk_driver_bo_get_led,
};

static trunk_cfp2dco_driver_cfg_t trunk_cfp2dco_driver_cfg =
{
    .nr_of_dco     = sizeof(cfp2_devs)/sizeof(trunk_cfp2dco_dev_inf_t),
    .dco_devs      = cfp2_devs,
    .ports_per_device = 1,
    .client_type       = FLEXCOH_TYPE_AC200,
    .mdio_ctx_build    = mdio_build_io_ctx,
    .mxp1_i2c_ctx_build= i2c_build_io_ctx,
    .mxp2_i2c_ctx_build= i2c_build_io_ctx,
    .cfp2dco_glue_cb   = &trunk_bo_glue_lib_cb,
};


flexcoh_ret_code trunk_driver_bo_glue_lib_init (void ** ctx)
{
    flexcoh_ret_code rc = FLEXCOH_EOK;
    bool nta = false;
    static trunk_cfp2dco_driver_cfg_t dev_cfg;
    static trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;
    static uint8_t nr_of_cfp2_devs =0;

    dev_cfg = trunk_cfp2dco_driver_cfg;

    nr_of_cfp2_devs =  dev_cfg.nr_of_dco;

    // init hw device ctx
    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t*)malloc(sizeof(trunk_cfp2dco_ctx_t) * nr_of_cfp2_devs);
    memset(trunk_cfp2dco_ctx, 0, sizeof(trunk_cfp2dco_ctx_t) * nr_of_cfp2_devs);

    //Initialize the MDIO driver for building MDIO context
    if (LC_EOK != mdio_driver_init(&mdio_config))
        return FLEXCOH_ERR_FAILURE;

    if ((LC_EOK != mdio_driver_start(&mdio_config, nta)) )
        return FLEXCOH_ERR_FAILURE;

    //Initialize the I2C driver for building I2C context
    if (LC_EOK != i2c_driver_init(&i2c_config))
        return FLEXCOH_ERR_FAILURE;

    if ((LC_EOK != i2c_driver_start(&i2c_config, nta)) )
        return FLEXCOH_ERR_FAILURE;

    int ii;
    for (ii = 0; ii < nr_of_cfp2_devs; ++ii) {

        // init trunk device ctx
        trunk_cfp2dco_ctx[ii].hw_trunk_dev_id = dev_cfg.dco_devs[ii].dev_id;
        trunk_cfp2dco_ctx[ii].mdio_hw_id = dev_cfg.dco_devs[ii].mdio_hw_id;
        trunk_cfp2dco_ctx[ii].mxp1_i2c_hw_id = dev_cfg.dco_devs[ii].mxp1_i2c_hw_id;
        trunk_cfp2dco_ctx[ii].mxp2_i2c_hw_id = dev_cfg.dco_devs[ii].mxp2_i2c_hw_id;
        trunk_cfp2dco_ctx[ii].flexcoh_handler = FLEXCOH_HANDLE_NO_HANDLE;

        // mdio ctx for trunk device data access
        trunk_cfp2dco_ctx[ii].mdio_ctx = calloc(1, sizeof(struct io_ctx));
        dev_cfg.mdio_ctx_build(trunk_cfp2dco_ctx[ii].mdio_ctx,
                                             dev_cfg.dco_devs[ii].mdio_hw_id,
                                             dev_cfg.dco_devs[ii].mdio_bus_id);

        //mxp1 i2c ctx for trunk device control access
        trunk_cfp2dco_ctx[ii].mxp1_i2c_ctx = calloc(1, sizeof(struct io_ctx));
        dev_cfg.mxp1_i2c_ctx_build(trunk_cfp2dco_ctx[ii].mxp1_i2c_ctx,
                                                   dev_cfg.dco_devs[ii].mxp1_i2c_bus_id,
                                                   dev_cfg.dco_devs[ii].mxp1_i2c_hw_id);

        // mxp2 i2c ctx for trunk device control access
        trunk_cfp2dco_ctx[ii].mxp2_i2c_ctx = calloc(1, sizeof(struct io_ctx));
        dev_cfg.mxp2_i2c_ctx_build(trunk_cfp2dco_ctx[ii].mxp2_i2c_ctx,
                                                   dev_cfg.dco_devs[ii].mxp2_i2c_bus_id,
                                                   dev_cfg.dco_devs[ii].mxp2_i2c_hw_id);

        trunk_cfp2dco_ctx[ii].flexcoh_handler = ii;

    }

    REFLEX_INFO("{ss}",MSG_CFP2,"Glue Lib init end ");

    //update the context for SDK reference
    *ctx = trunk_cfp2dco_ctx;

    return rc;
}
/*********** END ------ BO GLUE LIB INIT For FlexCOH SDK --------- *************/

flexcoh_ret_code trunk_driver_bo_read_reg (void * ctx, uint32_t address, uint32_t *data)
{
    trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;
    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t *)ctx;
    uint32_t mdio_address = 0;

    if ((!trunk_cfp2dco_ctx) || (!trunk_cfp2dco_ctx->mdio_ctx))
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "context NULL");
        return FLEXCOH_ERR_FAILURE;
    }

    if (!trunk_cfp2dco_ctx->mdio_ctx->io_read)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "read context NULL");
        return FLEXCOH_ERR_FAILURE;
    }
    // Check to be added later
   /*
    if(trunk_cfp2dco_ctx->dev_status != TRUNK_DEV_INSERTED)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "plug not inserted");
        return FLEXCOH_ERR_FAILURE;
    }
    */

    mdio_address = address | 0x10000; //hack for mdio reg check, to be deleted later

    drverrno rc = trunk_cfp2dco_ctx->mdio_ctx->io_read(trunk_cfp2dco_ctx->mdio_ctx, mdio_address, data, 1);
    if (rc) {
        //REFLEX_ERROR("{sssi}",MSG_CFP2,"mdio read failed", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }

#ifdef BO_GLUE_TRACE
    uint8_t dev_id = trunk_cfp2dco_ctx->hw_trunk_dev_id;
    REFLEX_INFO("{sssisisisi}", MSG_CFP2, "mdio read reg for ",
                "dev_id", dev_id, "data", *data, "address:", address,
                "rc:", rc);
#endif
    return FLEXCOH_EOK;
}


flexcoh_ret_code trunk_driver_bo_write_reg (void * ctx, uint32_t address, uint32_t data)
{
    trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;
    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t *)ctx;
    uint32_t mdio_address = 0;

    if ((!trunk_cfp2dco_ctx) || (!trunk_cfp2dco_ctx->mdio_ctx))
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "context NULL");
        return FLEXCOH_ERR_FAILURE;
    }

    if (!trunk_cfp2dco_ctx->mdio_ctx->io_write)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "write context NULL");
        return FLEXCOH_ERR_FAILURE;
    }
    // Check to be added later
   /*
    if(trunk_cfp2dco_ctx->dev_status != TRUNK_DEV_INSERTED)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "plug not inserted");
        return FLEXCOH_ERR_FAILURE;
    }
    */


    mdio_address = address | 0x10000; //hack for mdio reg check, to be deleted later

    drverrno rc = trunk_cfp2dco_ctx->mdio_ctx->io_write(trunk_cfp2dco_ctx->mdio_ctx, mdio_address, &data, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mdio write failed", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }

#ifdef BO_GLUE_TRACE
    uint8_t dev_id = trunk_cfp2dco_ctx->hw_trunk_dev_id;
    REFLEX_INFO("{sssisisisi}", MSG_CFP2, "mdio write reg for ",
                "dev_id", dev_id, "data", data, "address:", address,
                "rc:", rc);
#endif

    return FLEXCOH_EOK;
}

static inline uint32_t trunk_cfp2dco_pin_get(uint32_t word, uint32_t pin)
{
    return(word & (uint32_t)(1 << pin));
}

static inline uint32_t trunk_cfp2dco_pin_set(uint32_t toggle, uint32_t word, uint32_t pin)
{
    return(toggle ? (word | (uint32_t)(1 << pin)) : (word & ~(uint32_t)(1 << pin)));
}

flexcoh_ret_code trunk_driver_bo_read_status_pin (void * ctx, uint32_t *data)
{
    uint32_t data_rd = 0;
    uint32_t data_rd1 = 0;
    uint32_t port_0, port_1;
    drverrno rc = FLEXCOH_EOK;
    trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;

    *data = 0;
    port_0 = 0;
    port_1 = 1;

    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t *)ctx;

    if (!trunk_cfp2dco_ctx)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "context NULL in read_status");
        return FLEXCOH_ERR_FAILURE;
    }

   // read mxp1 port 1
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_1, &data_rd, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mxp1 port1 read failed", "Return Code: ",  rc);
        return FLEXCOH_ERR_FAILURE;
    }

    if (!(data_rd & 0x01)) // P8 (1st bit) CFP2 Glb Alrm
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_GLB_ALRM);
    if (data_rd & 0x02) // P9 (2nd bit) CFP2 MOD ABS
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_MOD_ABS);
    if (data_rd & 0x04) // P10 (3rd bit) CFP2 PWR GOOD
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PWR_OK);
    if (data_rd & 0x08) // P11 (4th bit) CFP2 PRG ALRM3
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PRG_ALRM3);
    if (data_rd & 0x10) // P12 (5th bit) CFP2 PRG ALRM2
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PRG_ALRM2);
    if (data_rd & 0x20) // P13 (6th bit) CFP2 PRG ALRM1
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PRG_ALRM1);

   // read mxp2 port 0
    rc = trunk_cfp2dco_ctx->mxp2_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp2_i2c_ctx, port_0, &data_rd1, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mxp2 port0 read failed", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }

    if (data_rd1 & 0x20) // P5 (6th bit) RX LOS
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_RX_LOS);

#ifdef BO_GLUE_TRACE
    uint8_t dev_id = trunk_cfp2dco_ctx->hw_trunk_dev_id;
    REFLEX_INFO("{sssisisisi}", MSG_CFP2, "read status pin for ",
                "dev_id", dev_id, "data_rd", data_rd, "data_rd1:", data_rd1,
                "*data:", *data);
#endif

    return FLEXCOH_EOK;
}

flexcoh_ret_code trunk_driver_bo_read_ctrl_pin (void * ctx, uint32_t *data)
{
    uint32_t data_rd = 0;
    uint32_t data_rd1 = 0;
    uint32_t port_0, port_1;
    drverrno rc = FLEXCOH_EOK;
    trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;

    *data = 0;
    port_0 = 2;
    port_1 = 3;

    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t *)ctx;

    if (!trunk_cfp2dco_ctx)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "context NULL in read_ctrl");
        return FLEXCOH_ERR_FAILURE;
    }

   // read mxp1 port 0
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_0, &data_rd, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2, "mxp1 port0 read failed", "Return Code: ",  rc);
        return FLEXCOH_ERR_FAILURE;
    }

    // 0 is reset and 1 is out of reset
    if (!(data_rd & 0x01)) // P0 (1st bit) CFP2 MOD RST
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_MOD_RSTN);
    if (data_rd & 0x02) // P1 (2nd bit) CFP2 MOD LOPWR
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_MOD_LWPR);
    if (data_rd & 0x20) // P5 (6th bit) CFP2 PRG CNTL3
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PRG_CNTL3);
    if (data_rd & 0x40) // P6 (7th bit) CFP2 PRG CNTL2
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PRG_CNTL2);
    if (data_rd & 0x80) // P7 (8th bit) CFP2 MOD POWER ENABLE, 0= Enable, 1= Disable
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PWR_DIS);

   // read mxp1 port 1
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_1, &data_rd1, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2, "mxp1 port1 read failed.", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }

    if (data_rd1 & 0x40) // P14 (7th bit) CFP2 TX DIS
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_TX_DIS);
    if (data_rd1 & 0x80) // P15 (8th bit) CFP2 PRG CNTL1
        *data = trunk_cfp2dco_pin_set(1, *data, FLEXCOH_AC200_PIN_PRG_CNTL1);

#ifdef BO_GLUE_TRACE
    uint8_t dev_id = trunk_cfp2dco_ctx->hw_trunk_dev_id;
    REFLEX_INFO("{sssisisisi}", MSG_CFP2, "read ctrl pin for ",
                "dev_id", dev_id, "data_rd", data_rd, "data_rd1:", data_rd1,
                "*data:", *data);
#endif

    return FLEXCOH_EOK;
}


flexcoh_ret_code trunk_driver_bo_write_ctrl_pin (void * ctx, uint32_t data)
{
    uint32_t data_wr = 0;
    uint32_t data_wr1 = 0;
    uint32_t data_dir = 0;//P0 to P7, all set to 0 for o/p
    uint32_t data_dir1 = 0x3F; //P14 and P15 set to 0 for o/p
    drverrno rc = FLEXCOH_EOK;
    uint32_t port_0, port_1;

    trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;

    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t *)ctx;

    if (!trunk_cfp2dco_ctx)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "context NULL in write ctrl");
        return FLEXCOH_ERR_FAILURE;
    }

    /* first set the direction bits */
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_write(trunk_cfp2dco_ctx->mxp1_i2c_ctx, 6, &data_dir, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2, "data_dir write failed", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_write(trunk_cfp2dco_ctx->mxp1_i2c_ctx, 7, &data_dir1, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"data_dir1 write failed", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }

    port_0 = 2;
    port_1 = 3;

    //First read the current value
    // read mxp1 port 0
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_0, &data_wr, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mxp1 port0 read failed in write ctrl", "Return Code", rc);
        return LC_ERROR;
    }
#ifdef BO_GLUE_TRACE
    uint8_t dev_id = trunk_cfp2dco_ctx->hw_trunk_dev_id;
    REFLEX_INFO("{sisi}", "current value in write ctrl for dev_id: ",
                dev_id, "data read is:", data_wr);
#endif

    // 0 is reset and 1 is out of reset
    if (trunk_cfp2dco_pin_get(data, FLEXCOH_AC200_PIN_MOD_RSTN) == 0)
        data_wr |= 0x01; // P0 (1st bit) CFP2 MOD RST
    else
        data_wr &= ~(0x01);
    if (trunk_cfp2dco_pin_get(data, FLEXCOH_AC200_PIN_MOD_LWPR) != 0)
        data_wr |= 0x02; // P1 (2nd bit) CFP2 MOD LOPWR
    else
        data_wr &= ~(0x02);
    if (trunk_cfp2dco_pin_get(data, FLEXCOH_AC200_PIN_PRG_CNTL3) != 0)
        data_wr |= 0x20; // P5 (6th bit) CFP2 PRG CNTL3
    else
        data_wr &= ~(0x20);
    if (trunk_cfp2dco_pin_get(data, FLEXCOH_AC200_PIN_PRG_CNTL2) != 0)
        data_wr |= 0x40; // P6 (7th bit) CFP2 PRG CNTL2
    else
        data_wr &= ~(0x40);
    if (trunk_cfp2dco_pin_get(data, FLEXCOH_AC200_PIN_PWR_DIS) != 0)
        data_wr |= 0x80; // P7 (8th bit) CFP2 MOD POWER ENABLE, 0= Enable, 1= Disable
    else
        data_wr &= ~(0x80);

   // write mxp1 port 0
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_write(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_0, &data_wr, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2, "mxp0 port1 write failed", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }

    //First read the current value
    // read mxp1 port 1
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_1, &data_wr1, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mxp1 port0 read failed in write ctrl", "Return Code", rc);
        return LC_ERROR;
    }

#ifdef BO_GLUE_TRACE
    REFLEX_INFO("{sisi}", "current value of mxp1 port 1 in write ctrl for dev_id: ",
                dev_id, "data read is:", data_wr1);
#endif

    if (trunk_cfp2dco_pin_get(data, FLEXCOH_AC200_PIN_TX_DIS) != 0)
        data_wr1 |= 0x40; // P14 (7th bit) CFP2 TX DIS
    else
        data_wr1 &= ~(0x40);
    if (trunk_cfp2dco_pin_get(data, FLEXCOH_AC200_PIN_PRG_CNTL1) != 0)
        data_wr1 |= 0x80; // P15 (8th bit) CFP2 PRG CNTL1
    else
        data_wr1 &= ~(0x80);

   // write mxp1 port 1
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_write(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_1, &data_wr1, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2, "mxp1 port1 write failed", "Return Code: ", rc);
        return FLEXCOH_ERR_FAILURE;
    }

#ifdef BO_GLUE_TRACE
    REFLEX_INFO("{sssisisisi}", MSG_CFP2, "mxp glue write ctrl pin for ",
                "dev_id", dev_id, "data", data, "data_wr:", data_wr,
                "data_wr1:", data_wr1);
#endif

    return FLEXCOH_EOK;
}

lc_errno trunk_driver_bo_set_led (void * ctx, uint8_t ledstat)
{
    lc_errno rc = LC_EOK;
    uint32_t port_0 = 0;
    uint32_t data = 0;
    trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;

    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t *)ctx;


    if (!trunk_cfp2dco_ctx)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "context NULL in set led");
        return LC_ERROR;
    }

    port_0 = 2;

    //First read the current value
    // read mxp1 port 0
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_0, &data, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mxp1 port0 read failed in led set", "Return Code ", rc);
        return LC_ERROR;
    }

#ifdef BO_GLUE_TRACE
    uint8_t dev_id = trunk_cfp2dco_ctx->hw_trunk_dev_id;
    REFLEX_INFO("{sssisisisi}", MSG_CFP2, "set led for ",
                "dev_id", dev_id, "ledstat", ledstat, "current data read is:", data,
                "rc:", rc);
#endif

    /* PIN references for LED
     * P2 = LED G L
     * P3 = LED R L
     * P4 = BLINK EN
     */
    switch (ledstat) {
        case LED_OFF:
            // set P2 and P3
            data |= (1 << BO_TRUNK_LED_RED_POS);
            data |= (1 << BO_TRUNK_LED_GREEN_POS);
            // And also clear P4, 5th Bit
            data &= ~(1 << BO_TRUNK_LED_BLINK_POS);
            break;
        case LED_GREEN:
            //clear P2, 3rd bit
            data &= ~(1 << BO_TRUNK_LED_GREEN_POS);
            // Also clear other alarms bits, set P3 and reset P4
            data |= (1 << BO_TRUNK_LED_RED_POS);
            data &= ~(1 << BO_TRUNK_LED_BLINK_POS);
            break;
        case LED_YELLOW:
            //clear both P2 and P3
            data &= ~(1 << BO_TRUNK_LED_RED_POS);
            data &= ~(1 << BO_TRUNK_LED_GREEN_POS);
            // And also clear P4, 5th Bit
            data &= ~(1 << BO_TRUNK_LED_BLINK_POS);
            break;
        case LED_AMBER_BLINK:
            // this is yellow(both red and green) + Blink
            //clear both P2 and P3
            data &= ~(1 << BO_TRUNK_LED_GREEN_POS);
            data &= ~(1 << BO_TRUNK_LED_RED_POS);
            // And also set P4, 5th Bit
            data |= (1 << BO_TRUNK_LED_BLINK_POS);
            break;
        case LED_RED:
            //clear P3, 4th bit
            data &= ~(1 << BO_TRUNK_LED_RED_POS);
            // Also clear other alarms bits, set P2 and reset P4
            data |= (1 << BO_TRUNK_LED_GREEN_POS);
            data &= ~(1 << BO_TRUNK_LED_BLINK_POS);
            break;
        default:
            REFLEX_PROV_ERR("{ss}", MSG_CFP2, "Invalid LED colour requested");
            return(LC_EINVAL);
    }


    // write updated value in mxp1 port 0
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_write(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_0, &data, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mxp1 port0 write failed in set led", "Return Code", rc);
        return LC_ERROR;
    }
#ifdef BO_GLUE_TRACE
    REFLEX_INFO("{sssisisisi}", MSG_CFP2, "set led for ",
                "dev_id", dev_id, "data", data, "ledstat:", ledstat,
                "rc:", rc);
#endif

    return rc;
}

lc_errno trunk_driver_bo_get_led (void * ctx, uint8_t *ledstat)
{
    lc_errno rc = LC_EOK;
    uint32_t port_0 = 0;
    uint32_t data_rd = 0;
    uint32_t data_led = 0;
    trunk_cfp2dco_ctx_t *trunk_cfp2dco_ctx;

    trunk_cfp2dco_ctx = (trunk_cfp2dco_ctx_t *)ctx;


    if (!trunk_cfp2dco_ctx)
    {
        REFLEX_ERROR("{ss}",MSG_CFP2, "context NULL in set led");
        return LC_ERROR;
    }

    //here 2 is ioexpander output register
    port_0 = 2;

    // read mxp1 port 0
    rc = trunk_cfp2dco_ctx->mxp1_i2c_ctx->io_read(trunk_cfp2dco_ctx->mxp1_i2c_ctx, port_0, &data_rd, 1);
    if (rc) {
        REFLEX_ERROR("{sssi}",MSG_CFP2,"mxp1 port0 read failed in led get", "Return Code", rc);
        return LC_ERROR;
    }

    /* check the bits for LED
     * P2 = LED G L
     * P3 = LED R L
     * P4 = BLINK EN
     */

    /*Making data_led in simpler form for LED bits calculations
    Since we are only concerned for P2, P3 and P4 so data_rd
    is right shifted by 2 and masked by 7 so that, need to check only first 3 bits.
    Now after shifting and masking, P0=Green, P1=Red, P2=Blink
    */
    data_led = ((data_rd >> 2) & 0x7);

#ifdef BO_GLUE_TRACE
    uint8_t dev_id = trunk_cfp2dco_ctx->hw_trunk_dev_id;
    REFLEX_INFO("{sisisi}", "get led for dev_id: ",
                dev_id, "current data read is:", data_rd, "data_led: ", data_led);
#endif
    switch (data_led) {
    case BO_TRUNK_LED_STATE_YELLOW:
        *ledstat = LED_YELLOW;
        break;
    case BO_TRUNK_LED_STATE_RED:
        *ledstat = LED_RED;
        break;
    case BO_TRUNK_LED_STATE_GREEN:
        *ledstat = LED_GREEN;
        break;
    case BO_TRUNK_LED_STATE_OFF:
        *ledstat = LED_OFF;
        break;
    case BO_TRUNK_LED_STATE_AMBER_BLINK:
        *ledstat = LED_AMBER_BLINK;
        break;
    default:
        REFLEX_PROV_ERR("{ss}", MSG_CFP2, "Invalid LED colour ");
        return(LC_EINVAL);
    }

#ifdef BO_GLUE_TRACE
    REFLEX_INFO("{sisi}", "get led for dev_id: ",
                dev_id, "ledstat is:", *ledstat);
#endif
    return rc;
}
