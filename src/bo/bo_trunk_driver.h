/*-----------------------------------------------------------------------
 * bo_trunk_driver.h
 *
 * Feb, 2019, BO Team
 *
 * Copyright (c) 2018-2019 by Cisco Systems, Inc.
 *
 * LC application Trunk driver BO
 *-----------------------------------------------------------------------
 */


#ifndef BHLCPD_TRUNK_DRIVER_BO_H_
#define BHLCPD_TRUNK_DRIVER_BO_H_

#include "trunk_cfp2dco_driver.h"
#include "flexcoh_types.h"
#include "flexcoh_api.h"
#include "flexcoh_ac200.h"
#include "flexcoh_errno.h"

#define BO_TRUNK_LED_GREEN_POS  2
#define BO_TRUNK_LED_RED_POS    3
#define BO_TRUNK_LED_BLINK_POS  4


/* Pin Details for LED
 * P2 = LED G L
 * P3 = LED R L
 * P4 = BLINK EN
 * 1. Green LED and RED LED pin is active low
 * 2. Blink enable pin is active high

    To make calculation simpler for LED bits, shifting of bits is
    done in such a way that need to check only first 3 bits
    (check Glue Led set and get function for details).
    Now after shifting and masking, P0=Green, P1=Red, P2=Blink
    Calculate now the bits for different LED Colors
                 Blink(P2) Red(P1) Green(P0)
    1- Yellow -- 000, Both P0 and P1 Low to ON Green and Red, and P2 also low
    2- Red    -- 001 (Blink Low, Red Low to ON , Green High to OFF
    3- Green  -- 010 (Blink Low, Red High to OFF, Green Low to ON
    4- OFF    -- 011 (Blink Low, Red and Green High to OFF)
    5- Blink  -- 100 (Blink High, Red and Green Low to ON)
 */
#define BO_TRUNK_LED_STATE_YELLOW      0x0
#define BO_TRUNK_LED_STATE_RED         0x1
#define BO_TRUNK_LED_STATE_GREEN       0x2
#define BO_TRUNK_LED_STATE_OFF         0x3
#define BO_TRUNK_LED_STATE_AMBER_BLINK 0x4

typedef enum {
    TRUNK_DCO_PORT_0 = 12,
    TRUNK_DCO_PORT_1,
    TRUNK_DCO_PORT_MAX
} trunk_cfp2dco_port_index_e;


flexcoh_ret_code trunk_driver_bo_glue_lib_init (void ** ctx);

flexcoh_ret_code trunk_driver_bo_read_reg (void * ctx, uint32_t address, uint32_t *data);

flexcoh_ret_code trunk_driver_bo_write_reg (void * ctx, uint32_t address, uint32_t data);

flexcoh_ret_code trunk_driver_bo_read_status_pin (void * ctx, uint32_t *data);

flexcoh_ret_code trunk_driver_bo_read_ctrl_pin (void * ctx, uint32_t *data);

flexcoh_ret_code trunk_driver_bo_write_ctrl_pin (void * ctx, uint32_t data);

lc_errno trunk_driver_bo_set_led (void * ctx, uint8_t ledstat);

lc_errno trunk_driver_bo_get_led (void * ctx, uint8_t *ledstat);

#endif /* BHLCPD_TRUNK_DRIVER_BO_H_ */
