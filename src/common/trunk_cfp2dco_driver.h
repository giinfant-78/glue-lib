/*-----------------------------------------------------------------------
 * trunk_cfp2dco_driver.h
 *
 * Feb, 2019, BO Team
 *
 * Copyright (c) 2018-2019 by Cisco Systems, Inc.
 *
 * LC application Trunk CFP2DCO driver
 *-----------------------------------------------------------------------
 */


#ifndef BHLCPD_TRUNK_CFP2DCO_DRIVER_H_
#define BHLCPD_TRUNK_CFP2DCO_DRIVER_H_

#include "app_common.h"
#include "lc_common_types.h"
#include "flexcoh_types.h"
#include "drv_descriptor.h"
#include "flexcoh_ac200.h"
#include <math.h>

#define CONVERT_AC200_hDBm_TO_10thuW(power) (10000 * (pow(10,((float)power * 0.001f))))

#define CONVERT_AC200_10thuW_to_hdBm(power) (10 * (log10((float)power * 0.001f)))

#define CONVERT_AC200_bias_TO_100PC(bias) ((10000 * bias)/ 65535)

#define  TRUNK_CFP2DCO_INV_MAX_LENGTH            128
#define MSG_CFP2 "cfp2dco"

/* Function Pointers */
typedef lc_errno (*trunk_cfp2dco_set_led) (void * ctx, uint8_t ledstat);
typedef lc_errno (*trunk_cfp2dco_get_led) (void * ctx, uint8_t *ledstat);

enum trunk_cfp2dco_diag_poke_op {

    TRUNK_CFP2DCO_DIAG_POKE_OP_LIST = 0,
    TRUNK_CFP2DCO_DIAG_SET_LASER_ENABLE,
    TRUNK_CFP2DCO_DIAG_SET_TRAFFIC_TYPE,
    TRUNK_CFP2DCO_DIAG_SET_LED,
    TRUNK_CFP2DCO_DIAG_SET_CLIENT_FACILITY_LB,
    TRUNK_CFP2DCO_DIAG_SET_CLIENT_TERMINAL_LB,
    TRUNK_CFP2DCO_DIAG_SET_TRUNK_TERMINAL_LB,
    TRUNK_CFP2DCO_DIAG_SET_TRUNK_FACILITY_LB,
    TRUNK_CFP2DCO_DIAG_POKE_OP_MAX
};

enum trunk_cfp2dco_diag_peek_op {

    TRUNK_CFP2DCO_DIAG_PEEK_OP_LIST = 0,
    TRUNK_CFP2DCO_DIAG_REG_READ,
    TRUNK_CFP2DCO_DIAG_REG_WRITE,
    TRUNK_CFP2DCO_DIAG_STATUS_READ,
    TRUNK_CFP2DCO_DIAG_CTRL_READ,
    TRUNK_CFP2DCO_DIAG_CTRL_WRITE,
    TRUNK_CFP2DCO_DIAG_GET_PRESENCE,
    TRUNK_CFP2DCO_DIAG_GET_LASER,
    TRUNK_CFP2DCO_DIAG_GET_TRAFFIC,
    TRUNK_CFP2DCO_DIAG_TEMPERATURE_DUMP,
    TRUNK_CFP2DCO_DIAG_ENABLE_ALARM_NOTIFY,
    TRUNK_CFP2DCO_DIAG_ENABLE_PM_NOTIFY,
    TRUNK_CFP2DCO_DIAG_SIMULATE_OIR,
    TRUNK_CFP2DCO_DIAG_GET_LED,
    TRUNK_CFP2DCO_DIAG_SET_BUS,
    TRUNK_CFP2DCO_DIAG_GET_FREQ,
    TRUNK_CFP2DCO_DIAG_GET_CDMIN,
    TRUNK_CFP2DCO_DIAG_GET_CDMAX,
    TRUNK_CFP2DCO_DIAG_GET_LB_STATUS,
    TRUNK_CFP2DCO_DIAG_PEEK_OP_MAX
};

typedef struct __trunk_cfp2dco_inventory_inf_rec_t__ {
    char deviation[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char part_number[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char serial_number[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char date_code_number[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char CLEI_code_number[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char vendorname[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char description[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char pid[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char vid[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char cisco_part_number[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char cisco_serial_number[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    char cisco_rev[TRUNK_CFP2DCO_INV_MAX_LENGTH];
    uint32_t oif_aco_bool;
    uint16_t module_hardware_version_number;
    uint16_t module_firmware_running_version_number;
    uint16_t module_firmware_running_build_number;
    uint16_t module_firmware_committed_version_number;
    uint16_t module_firmware_committed_build_number;
    uint32_t cisco_supplied_vendor_id;
    uint32_t ctc_type;
    uint32_t eac_type;
} trunk_cfp2dco_inventory_inf_rec_t;

typedef enum trunk_cfp2dco_status_ {
    TRUNK_DEV_UNKNOWN = 0,
    TRUNK_DEV_NOT_READY, //for error condition
    TRUNK_SDK_INIT_STARTED,//during start of init routine
    TRUNK_SDK_INIT_DONE,// end of init routine and sdk init success
    TRUNK_SDK_AVAILABLE_DONE,// end of trunk driver startup routine
    TRUNK_DEV_REMOVED,// on plug removal
    TRUNK_DEV_INSERTED,//on plug insertion
} trunk_cfp2dco_status_e;

typedef struct trunk_cfp2dco_port_ctx_t__ {
    void                      *trunk_cfp2dco_ctx;
    uint8_t                   face_port_num;        // face plate port num
    uint8_t                   dev_port_num;         // num of port (device based, from 0)
    flexcoh_traffic_type_t  traffic_type;
    flexcoh_extended_traffic_type_t  ext_traffic_type;
    bool                      previous_netw_traffic_fine;   // for debug to trace whenever traffic hit
    flexcoh_set_client_x_trunk_t client_x_trunk;    // cross bar info
} trunk_cfp2dco_port_ctx_t;

typedef struct trunk_cfp2dco_ctx_t_ {
    pthread_mutex_t           lock;             // to be locked on variable params access (alm,pm,inv..)
    trunk_cfp2dco_status_e    dev_status;       // off/online
    uint8_t                   hw_trunk_dev_id;      // num of hw device
    uint8_t                   mdio_hw_id;             //for mdio bus access
    uint8_t                   mxp1_i2c_hw_id;             //for mxp1 i2c bus access
    uint8_t                   mxp2_i2c_hw_id;             //for mxp2 i2c bus access
    uint8_t                   num_of_trunk_ports;     // num of trunk ports on this device
    uint8_t                   wait_time;
    bool                      pm_data_updating;
    trunk_cfp2dco_port_ctx_t  *trunk_cfp2dco_port_ctx;
    struct io_ctx             *mdio_ctx;
    struct io_ctx             *mxp1_i2c_ctx;
    struct io_ctx             *mxp2_i2c_ctx;
    flexcoh_handler_t         flexcoh_handler;
    trunk_cfp2dco_inventory_inf_rec_t trunk_cfp2dco_inv_data;
    optical_status_st         opt_pm_data[FLEXCOH_AC200_OPT_PORT_NUM];
    dwdm_status_st            dwdm_pm_data[FLEXCOH_AC200_OPT_PORT_NUM];
    otn_otu_status_st         otu_status_data[FLEXCOH_AC200_OPT_PORT_NUM];
    otn_otu_stats_st          otu_stats_data[FLEXCOH_AC200_OPT_PORT_NUM];
    flexcoh_global_pm_t       flexcoh_ac200_global_pm_data;
    flexcoh_ac200_global_alarms_t flexcoh_ac200_global_alarms;
} trunk_cfp2dco_ctx_t;

typedef struct trunk_cfp2dco_dev_inf{
    uint8_t          face_port_id;
    uint8_t          dev_id;
    uint8_t          mdio_hw_id;
    uint8_t          mdio_bus_id;
    uint8_t          mxp1_i2c_hw_id;
    uint8_t          mxp1_i2c_bus_id;
    uint8_t          mxp2_i2c_hw_id;
    uint8_t          mxp2_i2c_bus_id;
}trunk_cfp2dco_dev_inf_t;

typedef struct trunk_hal_glue_callbacks {
    flexcoh_read_reg_co_fn              read_reg_cb;
    flexcoh_write_reg_co_fn             write_reg_cb;
    flexcoh_read_status_pin_co_fn       read_status_cb;
    flexcoh_read_ctrl_prog_pin_co_fn    read_ctrl_cb;
    flexcoh_write_ctrl_prog_pin_co_fn   write_ctrl_cb;
    trunk_cfp2dco_set_led               set_led_cb;
    trunk_cfp2dco_get_led               get_led_cb;
} trunk_hal_glue_callbacks_t;

typedef struct trunk_dco_driver_cfg{
    uint8_t                  nr_of_dco;
    trunk_cfp2dco_dev_inf_t  *dco_devs;
    uint8_t                  ports_per_device;
    flexcoh_dev_type_t       client_type;
    io_build_io_ctx          mdio_ctx_build;
    io_build_io_ctx          mxp1_i2c_ctx_build;
    io_build_io_ctx          mxp2_i2c_ctx_build;
    trunk_hal_glue_callbacks_t *cfp2dco_glue_cb;
}trunk_cfp2dco_driver_cfg_t;

typedef struct trunk_cfp2dco_traffic_type_cfg{
    bool is_valid;
    uint8_t dev_id;
    uint32_t network_port;
    flexcoh_traffic_type_t traffic_cfg;
}trunk_cfp2dco_traffic_type_cfg_t;

typedef struct trunk_cfp2dco_set_tx_enable_cfg{
    bool is_valid;
    laser_state_e old_status;
    laser_state_e status;
}trunk_cfp2dco_set_tx_enable_cfg_t;

typedef struct trunk_cfp2dco_set_tx_shutdown_cfg{
    bool is_valid;
    laser_shutdown_state_e old_status;
    laser_shutdown_state_e status;
}trunk_cfp2dco_set_tx_shutdown_cfg_t;

typedef struct trunk_cfp2dco_set_tx_pwr_cfg{
    bool is_valid;
    uint32_t old_tx_power;
    uint32_t tx_power;
}trunk_cfp2dco_set_tx_pwr_cfg_t;

typedef struct trunk_cfp2dco_dwdm_set_frequency_cfg{
    bool is_valid;
    uint32_t old_freq_mhz;
    uint32_t freq_mhz;
}trunk_cfp2dco_dwdm_set_frequency_cfg_t;

typedef struct trunk_cfp2dco_dwdm_set_cd_min_cfg{
    bool is_valid;
    int32_t old_cd_min;
    int32_t cd_min;
}trunk_cfp2dco_dwdm_set_cd_min_cfg_t;

typedef struct trunk_cfp2dco_dwdm_set_cd_max_cfg{
    bool is_valid;
    int32_t old_cd_max;
    int32_t cd_max;
}trunk_cfp2dco_dwdm_set_cd_max_cfg_t;

typedef struct trunk_cfp2dco_set_loopback_cfg{
    bool is_valid;
    flexcoh_loop_status_t lb_status;
}trunk_cfp2dco_set_loopback_cfg_t;

typedef struct trunk_cf2dco_set_tti_cfg {
    bool is_sent_tti_valid;
    bool is_expected_tti_valid;
    flexcoh_tti_det_mode_t tti_mode;
    char sent_tti_string[TTI_LENGTH];
    char expected_tti_string[TTI_LENGTH];
}trunk_cfp2dco_set_tti_cfg_t;

typedef struct trunk_cfp2dco_driver_cfg_db{
    pthread_mutex_t                            lock;
    trunk_cfp2dco_traffic_type_cfg_t           set_traffic_type;
    trunk_cfp2dco_set_tx_enable_cfg_t          set_tx_enable;
    trunk_cfp2dco_set_tx_shutdown_cfg_t        set_tx_shutdown;
    trunk_cfp2dco_set_tx_pwr_cfg_t             set_tx_pwr;
    trunk_cfp2dco_dwdm_set_frequency_cfg_t     set_dwdm_freq;
    trunk_cfp2dco_dwdm_set_cd_min_cfg_t        set_cd_min;
    trunk_cfp2dco_dwdm_set_cd_max_cfg_t        set_cd_max;
    trunk_cfp2dco_set_loopback_cfg_t           set_loopback_cfg;
    trunk_cfp2dco_set_tti_cfg_t                set_tti_cfg;
}trunk_cfp2dco_driver_cfg_db_t;

typedef enum {
    TRUNK_DCO_DEV_0,
    TRUNK_DCO_DEV_1,
    TRUNK_DCO_DEV_MAX
} trunk_cfp2dco_dev_index_e;

typedef struct trunk_cfp2dco_oir_cfg{
    uint8_t dev_id;
    flexcoh_handler_t handler;
    flexcoh_device_address_t address;
    flexcoh_plugged_t plugged_in;
}trunk_cfp2dco_oir_cfg_t;

int trunk_cfp2dco_driver_mod_init(void *);

int trunk_cfp2dco_driver_mod_exit(void);

int trunk_cfp2dco_driver_mod_start(void *, bool);

int trunk_cfp2dco_driver_mod_stop(void);

lc_errno trunk_cfp2dco_driver_diag_poke(uint8_t device_id,
                             uint8_t resource_id,
                             uint8_t op,
                             void** param,
                             uint8_t param_num);

lc_errno trunk_cfp2dco_driver_diag_peek(uint8_t device_id,
                             uint8_t resource_id,
                             uint8_t op,
                             void** param,
                             uint8_t *param_num);

#define trunk_cfp2dco_driver_OPTICAL_CALLBACK_LIST                              \
{                                                                         \
   (set_optics_tx_enable_f)           trunk_cfp2dco_driver_optics_set_tx_enable,         \
   (set_optics_tx_shutdown_enable_f)  trunk_cfp2dco_driver_optics_set_tx_shutdown,       \
   (get_optics_prv_tx_state_f)        trunk_cfp2dco_driver_optics_get_prv_tx_state,      \
   (update_optics_tx_f)               trunk_cfp2dco_driver_optics_update_tx,             \
   (set_optics_tx_power_f)            trunk_cfp2dco_driver_optics_set_tx_power,          \
   (get_optics_cfg_tx_power_f)        trunk_cfp2dco_driver_optics_get_cfg_tx_power,      \
   (get_optics_manufacturing_data_f)  trunk_cfp2dco_driver_optics_get_manufacturing_data,\
   (get_optics_ls_tx_state_f)         trunk_cfp2dco_driver_optics_get_ls_tx_state,       \
   (get_optics_status_f)              trunk_cfp2dco_driver_optics_get_status,            \
   (get_optics_alarms_f)              trunk_cfp2dco_driver_optics_get_alarms,            \
   (get_optics_tx_cfg_f)              trunk_cfp2dco_driver_optics_get_tx_cfg,            \
   (set_optics_defaults_f)            trunk_cfp2dco_driver_optics_set_default_cfgs,      \
}


#define trunk_cfp2dco_driver_DWDM_CB_LIST											\
{                                                                          \
    (set_dwdm_tx_enable_f)    trunk_cfp2dco_driver_optics_set_tx_enable,             \
    (get_dwdm_status_f)       trunk_cfp2dco_driver_optics_get_ls_tx_state,           \
    (set_dwdm_freq_f)         trunk_cfp2dco_driver_dwdm_set_frequency,              \
    (get_dwdm_freq_f)         trunk_cfp2dco_driver_dwdm_get_frequency,              \
    (set_dwdm_cd_min_f)       trunk_cfp2dco_driver_dwdm_set_cd_min,			       \
    (get_dwdm_cd_min_f)       trunk_cfp2dco_driver_dwdm_get_cd_min,                 \
    (set_dwdm_cd_max_f)       trunk_cfp2dco_driver_dwdm_set_cd_max,			       \
    (get_dwdm_cd_max_f)       trunk_cfp2dco_driver_dwdm_get_cd_max,                 \
    (set_dwdm_defaults_f)     trunk_cfp2dco_driver_dwdm_set_default_cfgs,           \
    (get_dwdm_alarms_f)       trunk_cfp2dco_driver_dwdm_get_alarms,                 \
    (get_dwdm_status_f)       trunk_cfp2dco_driver_dwdm_get_status,                 \
    (set_dwdm_rx_colorless_mode_f) trunk_cfp2dco_driver_dwdm_set_rx_colorless_mode, \
    (get_dwdm_rx_colorless_mode_f) trunk_cfp2dco_driver_dwdm_get_rx_colorless_mode,  \
    (set_dwdm_enh_sop_tol_mode_f)  trunk_cfp2dco_driver_dwdm_set_enh_sop_tol_mode,   \
    (get_dwdm_enh_sop_tol_mode_f)  trunk_cfp2dco_driver_dwdm_get_enh_sop_tol_mode,   \
    (set_dwdm_voa_mode_f)     trunk_cfp2dco_driver_dwdm_set_voa_mode,              \
    (get_dwdm_voa_mode_f)     trunk_cfp2dco_driver_dwdm_get_voa_mode,              \
    (set_dwdm_voa_ratio_f)     trunk_cfp2dco_driver_dwdm_set_voa_ratio,              \
    (get_dwdm_voa_ratio_f)     trunk_cfp2dco_driver_dwdm_get_voa_ratio,              \
    (set_dwdm_voa_value_f)    trunk_cfp2dco_driver_dwdm_set_voa_value,              \
    (get_dwdm_voa_value_f)    trunk_cfp2dco_driver_dwdm_get_voa_value,              \
    (set_dwdm_rolloff_f)      trunk_cfp2dco_driver_dwdm_set_rolloff_value,          \
    (get_dwdm_rolloff_f)      trunk_cfp2dco_driver_dwdm_get_rolloff_value,          \
    (set_dwdm_nleq_f)         trunk_cfp2dco_driver_dwdm_set_nleq_mode,              \
    (get_dwdm_nleq_f)         trunk_cfp2dco_driver_dwdm_get_nleq_mode,              \
    (set_dwdm_cpr_pol_weight_f)   trunk_cfp2dco_driver_dwdm_set_cpr_pol_weight,     \
    (get_dwdm_cpr_pol_weight_f)   trunk_cfp2dco_driver_dwdm_get_cpr_pol_weight,     \
    (set_dwdm_cpr_pol_gain_f)     trunk_cfp2dco_driver_dwdm_set_cpr_pol_gain,       \
    (get_dwdm_cpr_pol_gain_f)     trunk_cfp2dco_driver_dwdm_get_cpr_pol_gain,       \
    (set_dwdm_cpr_ext_win_size_f) trunk_cfp2dco_driver_dwdm_set_cpr_ext_win_size,   \
    (get_dwdm_cpr_ext_win_size_f) trunk_cfp2dco_driver_dwdm_get_cpr_ext_win_size,    \
    (set_dwdm_cpr_win_size_f)     trunk_cfp2dco_driver_dwdm_set_cpr_win_size,       \
    (get_dwdm_cpr_win_size_f)     trunk_cfp2dco_driver_dwdm_get_cpr_win_size,        \
}

/* Implementation of Trunk CFP2DCO Driver interface for optical endpoints */
lc_errno
trunk_cfp2dco_driver_optics_set_tx_enable(void *hw_ctx,
                                  laser_state_e old_status, laser_state_e status,
                                  char *node_name);
lc_errno
trunk_cfp2dco_driver_optics_set_tx_shutdown(void *hw_ctx,
                                     laser_shutdown_state_e old_status, laser_shutdown_state_e status,
                                     char *node_name);
lc_errno
trunk_cfp2dco_driver_optics_get_prv_tx_state(void *hw_ctx, bool *status,
                                             char *node_name);

lc_errno
trunk_cfp2dco_driver_optics_update_tx(void *hw_ctx,
                              uint8_t old_status, uint8_t status, char *node_name);
lc_errno
trunk_cfp2dco_driver_optics_set_tx_power(void *hw_ctx,
                                 uint32_t old_tx_power, uint32_t tx_power, char *node_name);
lc_errno
trunk_cfp2dco_driver_optics_get_cfg_tx_power(void *hw_ctx, uint32_t *tx_power, char *node_name);

lc_errno
trunk_cfp2dco_driver_optics_get_manufacturing_data(uint8_t port_num, void *manuf_data);

lc_errno
trunk_cfp2dco_driver_optics_get_ls_tx_state (void *hw_ctx, laser_state_e *status, char *node_name);

lc_errno
trunk_cfp2dco_driver_optics_get_status(void *hw_ctx, optical_status_st *status, char *node_name);

lc_errno
trunk_cfp2dco_driver_optics_get_alarms(void *hw_ctx, void *alarms, char *node_name);


lc_errno
trunk_cfp2dco_driver_optics_get_tx_cfg(void *hw_ctx,
                               uint8_t *status, bool *state, char *node_name);

lc_errno
trunk_cfp2dco_driver_optics_set_default_cfgs(void *hw_ctx, char *node_name);

lc_errno
trunk_cfp2dco_driver_set_led_status(uint8_t port_num,
                            enum cidl_bh_lc_xpndr_led_color led_color);
lc_errno
trunk_cfp2dco_driver_optics_get_led_status(uint8_t port_num,
                         enum cidl_bh_lc_xpndr_led_color *led_color);

/* Implementation of Trunk CFP2DCO Driver interface for DWDM endpoints */
lc_errno
trunk_cfp2dco_driver_dwdm_set_frequency(void *hw_ctx,
                                uint32_t old_freq_mhz, uint32_t freq_mhz, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_get_frequency(void *hw_ctx,
                                uint32_t *freq_mhz, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_set_cd_min(void *hw_ctx,
                             int32_t old_cd_min, int32_t cd_min, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_cd_min(void *hw_ctx, int32_t *cd_min, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_cd_max(void *hw_ctx,
                             int32_t old_cd_max, int32_t cd_max, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_get_cd_max(void *hw_ctx,
                             int32_t *cd_max, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_set_default_cfgs(void *hw_ctx, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_get_alarms(void *hw_ctx, void *alarms, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_get_status(void *hw_ctx, dwdm_status_st *dwdm_status, char *node_name);


/* Traffic Provisioning Functions declaration */

lc_errno
trunk_cfp2dco_driver_provision_traffic_type(uint8_t dev_id, uint32_t network_port,
                                                    flexcoh_traffic_type_t traffic_cfg);

lc_errno
trunk_cfp2dco_driver_manage_network_lane_tx (uint8_t dev_id, uint8_t network_lane, laser_state_e laser_state,
                                     laser_state_e *present_state, bool *need_restore);

void flexcoh_error_str(flexcoh_ret_code err, char * err_str, uint32_t maxsize);

lc_errno
trunk_cfp2dco_driver_set_fec(uint8_t dev_id, 
                             enum cidl_bh_lc_xpndr_fec_type fec_type);

lc_errno
trunk_cfp2dco_driver_otu_get_alarms (uint8_t dev, uint8_t lane, uint64_t *otu_alarms);

lc_errno
trunk_cfp2dco_driver_otu_get_status (uint8_t dev, uint8_t lane, otn_otu_status_st *otu_status);

lc_errno
trunk_cfp2dco_driver_otu_get_stats (uint8_t dev, uint8_t lane, otn_otu_stats_st *otu_stats);

lc_errno trunk_cfp2dco_driver_set_loopback_type (uint8_t dev_id,
        enum cidl_bh_lc_xpndr_loopback_type old_loopback_type,
        enum cidl_bh_lc_xpndr_loopback_type loopback_type, char *node_name);

lc_errno
trunk_cfp2dco_get_ppm_presence(uint8_t face_port, bool *ppm_present);

lc_errno
trunk_cfp2dco_get_devid_from_face_port (uint8_t face_port, uint8_t *dev_id);

lc_errno
trunk_cfp2dco_set_tx_tti(uint8_t dev_id, char *tx_tti,uint32_t tti_count, bool tti_act, char *node_name);

lc_errno
trunk_cfp2dco_set_exp_tti(uint8_t dev_id, char *exp_tti,uint32_t tti_count, bool tti_act, char *node_name);

bool trunk_cfp2dco_driver_is_cfg_changed(uint8_t dev_id ,
                                         uint32_t network_port,
                                         dwdm_cfg_st dwdm_status,
                                         otn_otu_cfg_st otu_status);

lc_errno
convert_flexcoh_fec_to_app_fec ( flexcoh_fec_mode_t flexcoh_fec_type,
                                 flexcoh_non_diff_t flexcoh_fec_diff_opt,
                                 enum cidl_bh_lc_xpndr_fec_type *app_fec_type);

/* Start of SUB-Sea functions prototypes */

lc_errno
trunk_cfp2dco_driver_dwdm_set_rx_colorless_mode (void *hw_ctx,
                                 uint32_t old_rx_colorless_mode, uint32_t rx_colorless_mode, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_rx_colorless_mode (void *hw_ctx, uint32_t *rx_colorless_mode, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_enh_sop_tol_mode (void *hw_ctx,
                                 uint32_t old_enh_sop_tol_mode, uint32_t enh_sop_tol_mode, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_enh_sop_tol_mode (void *hw_ctx, uint32_t *enh_sop_tol_mode, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_voa_mode(void *hw_ctx,
                                 uint32_t old_voa_mode, uint32_t voa_mode, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_voa_mode(void *hw_ctx, uint32_t *voa_mode, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_voa_ratio (void *hw_ctx,
                                 int32_t old_voa_ratio, int32_t voa_ratio, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_voa_ratio (void *hw_ctx, int32_t *voa_ratio, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_voa_value (void *hw_ctx,
                                 int32_t old_voa_value, int32_t voa_value, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_voa_value (void *hw_ctx, int32_t *voa_value, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_rolloff_value(void *hw_ctx,
                                 uint32_t old_rolloff_value, uint32_t rolloff_value, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_rolloff_value(void *hw_ctx, uint32_t *rolloff_value, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_nleq_mode(void *hw_ctx,
		                         uint32_t old_nleq_mode, uint32_t nleq_mode, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_nleq_mode(void *hw_ctx,  uint32_t *nleq_mode, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_cpr_pol_weight (void *hw_ctx,
		                        uint32_t old_cpr_pol_weight, uint32_t cpr_pol_weight, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_cpr_pol_weight(void *hw_ctx, uint32_t *cpr_pol_weight, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_cpr_pol_gain(void *hw_ctx,
		                        uint32_t old_cpr_pol_gain, uint32_t cpr_pol_gain, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_cpr_pol_gain(void *hw_ctx, uint32_t *cpr_pol_gain, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_cpr_ext_win_size(void *hw_ctx,
		                        uint32_t old_cpr_ext_win_size, uint32_t cpr_ext_win_size, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_cpr_ext_win_size(void *hw_ctx, uint32_t *cpr_ext_win_size, char *node_name);

lc_errno
trunk_cfp2dco_driver_dwdm_set_cpr_win_size(void *hw_ctx,
		                        uint32_t old_cpr_win_size, uint32_t cpr_win_size, char *node_name);
lc_errno
trunk_cfp2dco_driver_dwdm_get_cpr_win_size(void *hw_ctx, uint32_t *cpr_win_size, char *node_name);

/* End of SUB-Sea functions prototypes */

#endif /* BHLCPD_TRUNK_CFP2DCO_DRIVER_H_ */
